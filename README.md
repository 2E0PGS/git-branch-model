# Git Branching Model

Documentation for my Git branching workflow.

## This is now mainly based upon Gitflow

[Gitflow a sucessful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)

## Current branch names

* `master` (this is the stable branch, evergreen)
* `develop` (this is the unstable branch, evergreen)
* `f/` (feature branch, short lived)
* `h/` (hotfix branch, short lived)
* `r/` (release branch, short lived)
* `b/` (bugfix branch, short lived)

## Fast forward or merge

* For feature branches merging into the develop branch I use no-ff since it preserves the commit history and allows others to see how I went about my workflow.

Useful article I found a while ago on this topic: https://developer.atlassian.com/blog/2014/12/pull-request-merge-strategies-the-great-debate/

## To create a new fork, new repo or new branch?

I lean away from maintaining classic versions of programs in branches now since it's a lot better to fork or create a new repo for maintaining a "classic" variant of a program. This also keeps commit history cleaner.
